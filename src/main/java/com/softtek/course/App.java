package com.softtek.course;

/**
 * Hello world!
 *
 */
public class App 
{

    public static void hola(){
        System.out.println( "¡Hola!" );
    }

    public static void adios(){
        System.out.println( "¡Adiós!" );
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
